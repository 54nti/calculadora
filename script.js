// estaremos agregando a la pantalla lo que pulsemos
let agregar = (valor) => {
    document.getElementById('pantalla').value += valor;
}

let borrar = () => {
    document.getElementById('pantalla').value = '';
}

let calcular = () => {
    const pantalla = document.getElementById('pantalla').value;
    const res = eval(pantalla);
    document.getElementById('pantalla').value = res;
}
